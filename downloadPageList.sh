#!/bin/bash

# Update count so it starts at the currect number
count=$1;
#3401;

# Usage
# -filename--------start number-- inputfile --- output folder---
# sh downloadPageList.sh 6000 PageList6k.txt lists6k
# sh downloadPageList.sh 7000 PageList7k.txt lists7k
# sh downloadPageList.sh 8000 PageList8k.txt lists8k
# sh downloadPageList.sh 9000 PageList9k.txt lists9k
# sh downloadPageList.sh 10000 PageList10k.txt lists10k


if [ $# -eq 0 ]; then #Check for argument
    echo "please set number to start from in the file"
else
    echo "argument present, starting process"

    while read line;
    do
        echo $count
        filename=$count.html
        curl $line > $3/$filename
        count=$(($count+1))
    done < $2
fi
