import fs from 'fs';
import cheerio from 'cheerio';
const scrapeHTML = (html) => {
    let $ = cheerio.load(html);
    let links=[];
    $('a.ai_ii').each((index, value)=>{
        var link = $(value).attr('href');
        links.push(link);
    });
    return links;
}

const scrapeListsFromHTML = () => {
    fs.readdir('lists', async (err, fileList) => {
        console.log('available data', fileList);
        let fullList = [];
        let count = 0;
        await fileList.forEach(async (fileName, index) => {
            fs.readFile(`lists/${fileName}`, async (err, data) => {
                let list = await scrapeHTML(data, fileName);
                
                //TODO: save file
                saveToFile(fileName, list);
            });
        });
    });
}

const saveToFile = async (fileName, list) => {
    let outFile = `listscraped2/scraped-${fileName}.json`;
    let outList = await JSON.stringify(list);
    console.log("Outfile", outFile);
    
    fs.writeFileSync(outFile, outList);
}

scrapeListsFromHTML();


//fs.readFile(`scraped/lists/0.html`, (err, data) => {
    //scrapeHTML(data);
//});