// List of stories
//FROM
// https://search.literotica.com/?query=%20&page=1
//TO
// https://search.literotica.com/?query=%20&page=10368

import fs from 'fs';

const addToFile = (line, index) => {
    fs.appendFile('PageList10k.txt', line, function (err) {
        if (err) {
          console.log("ERROR", err);
        } else {
          // done
          console.log("ADD", index);
        }
    });
}

for(let a = 10000; a<10392; a++){ //all pages: 10368
    let number = "0000"+a;
    number = number.substr(number.length - 5);
    let line ="https://search.literotica.com/?query=%20&page=" + number + "\n";
    addToFile(line, number);
}