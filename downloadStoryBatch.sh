#!/bin/bash

# Usage
# -filename----------------inputfile
# -----------------------------------
# sh downloadStoryBatch.sh 0 list0000
# sh downloadStoryBatch.sh 5000 list0001
# sh downloadStoryBatch.sh 10000 list0002
# sh downloadStoryBatch.sh 15000 list0003
# sh downloadStoryBatch.sh 20000 list0004
# sh downloadStoryBatch.sh 25000 list0005
# sh downloadStoryBatch.sh 30000 list0006
# sh downloadStoryBatch.sh 35000 list0007
# sh downloadStoryBatch.sh 40000 list0008
# sh downloadStoryBatch.sh 45000 list0009
# sh downloadStoryBatch.sh 50000 list0010
# sh downloadStoryBatch.sh 55000 list0011
# sh downloadStoryBatch.sh 60000 list0012
# sh downloadStoryBatch.sh 65000 list0013
# sh downloadStoryBatch.sh 70000 list0014
# sh downloadStoryBatch.sh 75000 list0015
# sh downloadStoryBatch.sh 80000 list0016
# sh downloadStoryBatch.sh 85000 list0017

count=$1;
date "+%H:%M:%S"
while read line;
do
    echo $count
    curl $line > stories/story-$count.html
    count=$(($count+1))
done < storyLists5k/$2
